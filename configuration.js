class Configuration {

  constructor(settingsFile) {
    this.settingsFile = settingsFile;
    this.loadSettings(settingsFile);
  }

  loadSettings(settingsFile) {
    var read = require("read-data").sync;
    this.settings = read(settingsFile);
  }

  getScriptPathSetting() {
    return this.settings.scriptPath;
  }

  getProjectPathSetting() {
    return this.settings.projectPath;
  }

  getGeneratedProjectPathSetting(projectName) {
    var _ = require("underscore");
    var project = _.find(this.settings.projects, {projectName: projectName});
    if ( project && project.generatedProjectPath ){
      return project.generatedProjectPath
    } else {
      return null;
    }
  }

//DeployementChannel = Deployment in progress, not yet fully deployed
  getDeploymentChannelSetting(projectName) {
    var _ = require("underscore");
    var project = _.find(this.settings.projects, {projectName: projectName});
    if ( project && project.generatedProjectPath ){
      return project.deploymentChannel
    } else {
      return null;
    }
  }

//DeployedChannel = Deployement Completed
  getDeployedChannelSetting(projectName) {
    var _ = require("underscore");
    var project = _.find(this.settings.projects, {projectName: projectName});
    if ( project && project.generatedProjectPath ){
      return project.deployedChannel
    } else {
      return null;
    }
  }

  getModelPathSetting() {
    return this.settings.modelPath;
  }

  setScriptPathSetting(scriptPath) {
    this.settings.scriptPath = scriptPath;
    var write = require("write-data").sync;
    write(this.settingsFile, this.settings);
  }

  setProjectPathSetting(projectName, projectPath) {
    this.settings.projectPath = projectPath;
    var write = require("write-data").sync;
    write(this.settingsFile, this.settings);
  }

  setGeneratedProjectPathSetting(projectName, generatedProjectPath) {
    var _ = require("underscore");
    var project = _.find(this.settings.projects, {projectName: projectName});
    if ( project === undefined ) {
      project = {};
      project.projectName = projectName;
    }
    project.generatedProjectPath = generatedProjectPath;
    this.settings.projects = _.without(this.settings.projects, _.findWhere(this.settings.projects, {projectName: projectName}));
    this.settings.projects.push(project);
    var write = require("write-data").sync;
    write(this.settingsFile, this.settings);
  }

//DeployementChannel = Deployment in progress, not yet fully deployed
  setDeploymentChannelSetting(projectName, deploymentChannel) {
    var _ = require("underscore");
    var project = _.find(this.settings.projects, {projectName: projectName});
    if ( project === undefined ) {
      project = {};
      project.projectName = projectName;
    }
    project.deploymentChannel = deploymentChannel;
    this.settings.projects = _.without(this.settings.projects, _.findWhere(this.settings.projects, {projectName: projectName}));
    this.settings.projects.push(project);
    var write = require("write-data").sync;
    write(this.settingsFile, this.settings);
  }

//DeployedChannel = Deployement Completed
  setDeployedChannelSetting(projectName, deployedChannel) {
    var _ = require("underscore");
    var project = _.find(this.settings.projects, {projectName: projectName});
    if ( project === undefined ) {
      project = {};
      project.projectName = projectName;
    }
    project.deployedChannel = deployedChannel;
    this.settings.projects = _.without(this.settings.projects, _.findWhere(this.settings.projects, {projectName: projectName}));
    this.settings.projects.push(project);
    var write = require("write-data").sync;
    write(this.settingsFile, this.settings);
  }

  removeProject(projectName) {
    var _ = require("underscore");
    this.settings.projects = _.without(this.settings.projects, _.findWhere(this.settings.projects, {projectName: projectName}));
    var write = require("write-data").sync;
    write(this.settingsFile, this.settings);
  }

  setModelPathSetting(modelPath) {
    this.settings.modelPath = modelPath;
    var write = require("write-data").sync;
    write(this.settingsFile, this.settings);
  }
}

function readScriptPathSetting(settingsFile) {
  configuration = new Configuration(settingsFile);
  return configuration.getScriptPathSetting();
}

function readProjectPathSetting(settingsFile) {
  configuration = new Configuration(settingsFile);
  return configuration.getProjectPathSetting();
}

function readGeneratedProjectPathSetting(settingsFile, projectName) {
  configuration = new Configuration(settingsFile);
  return configuration.getGeneratedProjectPathSetting(projectName);
}

//DeployementChannel = Deployment in progress, not yet fully deployed
function readDeploymentChannelSetting(settingsFile, projectName) {
  configuration = new Configuration(settingsFile);
  return configuration.getDeploymentChannelSetting(projectName);
}

//DeployedChannel = Deployement Completed
function readDeployedChannelSetting(settingsFile, projectName) {
  configuration = new Configuration(settingsFile);
  return configuration.getDeployedChannelSetting(projectName);
}

function readModelPathSetting(settingsFile) {
  configuration = new Configuration(settingsFile);
  return configuration.getModelPathSetting();
}

function saveScriptPathSetting(settingsFile, scriptPath) {
  configuration = new Configuration(settingsFile);
  configuration.setScriptPathSetting(scriptPath);
}

function saveProjectPathSetting(settingsFile, projectPath) {
  configuration = new Configuration(settingsFile);
  configuration.setProjectPathSetting(projectPath);
}

function saveGeneratedProjectPathSetting(settingsFile, projectName, generatedProjectPath) {
  configuration = new Configuration(settingsFile);
  configuration.setGeneratedProjectPathSetting(projectName, generatedProjectPath);
}

//DeployementChannel = Deployment in progress, not yet fully deployed
function saveDeploymentChannelSetting(settingsFile, projectName, deploymentChannel) {
  configuration = new Configuration(settingsFile);
  configuration.setDeploymentChannelSetting(projectName, deploymentChannel);
}

//DeployedChannel = Deployement Completed
function saveDeployedChannelSetting(settingsFile, projectName, deployedChannel) {
  configuration = new Configuration(settingsFile);
  configuration.setDeployedChannelSetting(projectName, deployedChannel);
}

function deleteProject(settingsFile, projectName){
  configuration = new Configuration(settingsFile);
  configuration.removeProject(projectName);
}

function saveModelPathSetting(settingsFile, modelPath) {
  configuration = new Configuration(settingsFile);
  configuration.setModelPathSetting(modelPath);
}

exports.readScriptPathSetting = readScriptPathSetting
exports.readProjectPathSetting = readProjectPathSetting
exports.readGeneratedProjectPathSetting = readGeneratedProjectPathSetting
exports.readDeploymentChannelSetting = readDeploymentChannelSetting
exports.readDeployedChannelSetting = readDeployedChannelSetting
exports.readModelPathSetting = readModelPathSetting

exports.saveScriptPathSetting = saveScriptPathSetting
exports.saveProjectPathSetting = saveProjectPathSetting
exports.saveGeneratedProjectPathSetting = saveGeneratedProjectPathSetting
exports.saveDeploymentChannelSetting = saveDeploymentChannelSetting
exports.saveDeployedChannelSetting = saveDeployedChannelSetting
exports.saveModelPathSetting = saveModelPathSetting
exports.deleteProject = deleteProject
