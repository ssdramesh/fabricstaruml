const _ = require("underscore");
const path = require("path");
const spawn = require("child_process").spawn;
const configuration = require("./configuration");
const channelFolderPath = "/deployment/ChannelDetails";
const deployScriptsPath = "/scripts";
const deployScript = "/scripts/deployToHLFaaS.sh";

class ChaincodeDeployer {

  constructor(settingsFile) {
    this.settingsFile = settingsFile;
  }

  getGeneratedProjectPathFromModel(model) {
    var projectName = path.basename(model, path.extname(model));
    return configuration.readGeneratedProjectPathSetting(this.settingsFile, projectName);
  }

  transformChannelsForSelection(channels) {
    var channelsSelector = [];
    _.each(channels, function(element, index, list) {
      channelsSelector.push({
        text: element.split(".")[0] + element.split(".")[1],
        value: element
      });
    });
    return channelsSelector;
  }

  selectAndSaveDeploymentChannel(model) {
    var that = this;
    var projectName = path.basename(model, path.extname(model));
    var channelsFolderPath = this.getGeneratedProjectPathFromModel(model) + channelFolderPath;
    var channels = [];
    var ls = spawn("ls", ["-A1"], {
      cwd: channelsFolderPath,
      env: process.env
    });
    ls.stdout.on("data", function(data) {
      channels = data.toString().trim().split("\n");
      app.dialogs.showSelectRadioDialog("Select channel to deply", that.transformChannelsForSelection(channels))
        .then(function(buttonId, returnValue) {
          if (buttonId === "ok") {
            app.dialogs.showInfoDialog(returnValue);
            configuration.saveDeploymentChannelSetting(this.settingsFile, projectName, returnValue);
          } else {
            app.dialogs.showInfoDialog("User Canceled the Deployment");
            configuration.saveDeploymentChannelSetting(this.settingsFile, projectName, null);
          }
        })
    });
    ls.stderr.on("data", function(data) {
      app.dialogs.showErrorDialog(data.toString());
    });
    ls.on("close", function(code) {
      //Do nothing for now!
    });
  }

// TODO - Case for Different OS
  deploy(model) {
    var projectName = path.basename(model, path.extname(model));
    var projectFolder = configuration.readGeneratedProjectPathSetting(this.settingsFile, projectName);
    var script = projectFolder + deployScript;
    var channel = projectFolder + channelsFolderPath + configuration.readDeploymentChannelSetting(this.settingsFile, projectName);
    var child = spawn("bash", [script, channel], {
      cwd: deployScriptsPath,
      env: process.env
    });
    child.stdout.on("data", function(data) {
      app.dialogs.showInfoDialog("Deployment Completed ")
      configuration.saveDeployedChannelSetting(this.setingsFile, projectName, channel);
    });
    child.stderr.on("data", function(data) {
      app.dialogs.showErrorDialog(data.toString());
      configuration.saveDeployedChannelSetting(this.setingsFile, projectName, null);
    });
    child.on("close", function(code) {
      // Do nothing for now!!
    });
  }
}

function deploy(model, settingsFile) {
  var chaincodeDeployer = new ChaincodeDeployer(settingsFile);
  chaincodeDeployer.deploy(model, channelFileName);
}

function selectDeploymentChannel(model, settingsFile) {
  var chaincodeDeployer = new ChaincodeDeployer(settingsFile);
  chaincodeDeployer.selectAndSaveDeploymentChannel(model);
}

exports.deploy = deploy
exports.readChannel = selectDeploymentChannel
