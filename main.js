const path = require("path");
const read = require("read-data").sync;
const configuration = require("./configuration");
const chaincodeGenerator = require("./chaincode-generator");
const chaincodeDeployer = require("./chaincode-deployer");

const settingsFile = "/Users/i047582/Library/Application Support/StarUML/extensions/user/fabric/settings.json";

function changeScriptLocation() {
  var scriptPath = configuration.readScriptPathSetting(settingsFile);
  var confirmScriptChange = app.dialogs.showConfirmDialog("The generation scripts are currently located at: \n" + scriptPath + "\n Do you want to change?");
  if ( confirmScriptChange == "ok" ) {
    scriptPath = app.dialogs.showOpenDialog("New Location for Generation Script", null, null, { properties: ['openDirectory'] })[0];
  }
  configuration.saveScriptPathSetting(settingsFile, scriptPath);
}

function changeProjectLocation() {
  var projectPath = configuration.readProjectPathSetting(settingsFile);
  var confirmProjectChange = app.dialogs.showConfirmDialog("The projects are currently located at: \n" + projectPath + "\n Do you want to change?");
  if ( confirmProjectChange == "ok" ) {
    projectPath = app.dialogs.showOpenDialog("New Location for Projects", null, null, { properties: ['openDirectory'] })[0];
  }
  configuration.saveProjectPathSetting(settingsFile, projectPath);
}

function changeModelLocation() {
  var modelPath = configuration.readModelPathSetting(settingsFile);
  var confirmModelChange = app.dialogs.showConfirmDialog("The models are currently located at: \n" + modelPath + "\n Do you want to change?");
  if ( confirmModelChange == "ok" ) {
    modelPath = app.dialogs.showOpenDialog("New Location for Models", null, null, { properties: ['openDirectory'] })[0];
  }
  configuration.saveModelPathSetting(settingsFile, modelPath);
}

function generateChaincode() {
  var model = app.project.getFilename();
  if ( model == null ) {
    model = app.dialogs.showOpenDialog('Select a model of your business network to be generated', null, null, { properties: ['openFile'] })[0];
    app.project.load(model);
  }
  var generatedProjectPath = configuration.readGeneratedProjectPathSetting(settingsFile, path.basename(model,path.extname(model)));
  if ( generatedProjectPath ) {
    app.dialogs.showErrorDialog("This model file has already been generated at: " + generatedProjectPath + "\n Regeneration is NOT supported! \nDrop Project and build instead")
    return;
  }
  chaincodeGenerator.generate(model, settingsFile);
}

function dropChaincode() {
  var model = app.project.getFilename();
  if ( model == null ) {
    model = app.dialogs.showOpenDialog('Select a model of your business network to be dropped', null, null, { properties: ['openFile'] })[0];
    app.project.load(model);
  }
  // Check if project is already generated
  var generatedProjectPath = configuration.readGeneratedProjectPathSetting(settingsFile, path.basename(model,path.extname(model)));
  if ( generatedProjectPath ) {
    var confirmDrop = app.dialogs.showConfirmDialog("Are you sure?  All generated artifacts will be removed from your projects workspace!");
    if ( confirmDrop == "ok" ) {
      chaincodeGenerator.drop(model, settingsFile);
    }
  }
}

function deployChaincodeOnSAPCloudFoundry() {
  var model = app.project.getFilename();
  if ( model == null ) {
    var model = app.dialogs.showOpenDialog('Select a model of your business network to be deployed', null, null, { properties: ['openFile'] })[0];
    app.project.load(model);
  }

  var projectName = path.basename(model, path.extname(model));

  if ( !configuration.readGeneratedProjectPathSetting(setingsFile, projectName) ) {
    app.dialogs.showErrorDialog("The model of this business network is not yet generated as a chaincode project. \n Please use 'Build' option in 'Generate' menu");
    return;
  }

  var deployedChannelName = configuration.readDeployedChannelSetting(settingsFile, projectName);
  if ( deployedChannelName ) {
    app.dialogs.showErrorDialog("This business network is already deployed in channel: " + deployedChannelName + "\n Upgrade of existing deployments not yet supported!");
  } else {
    chaincodeDeployer.selectDeploymentChannel(model, settingsFile);
    chaincodeDeployer.deploy(model);
  }

}

function init() {
  app.commands.register('fabric:change-script-location', changeScriptLocation);
  app.commands.register('fabric:change-project-location', changeProjectLocation);
  app.commands.register('fabric:change-model-location', changeModelLocation);
  app.commands.register('fabric:generate-chaincode', generateChaincode);
  app.commands.register('fabric:drop-chaincode', dropChaincode);
  app.commands.register('fabric:deploy-sap',deployChaincodeOnSAPCloudFoundry);
}

exports.init = init
