# Hyperledger Fabric on SAP Cloud Platform
This is a small extension to enable model-based development of Hyperledger Fabric chaincode applications with SAP Cloud Platform Cloud Foundry Hyperledger Fabric service environment.  This extension can be used to speed up devOps but automatic chaincode generation, chaincode unit tests, deployment of chaincode on SAP Cloud Platform Cloud Foundry environment.  Youc an also generate sample data into deployed chaincodes and generate a working UI for accessing the chaincode data via its APIs.

# Pre-requisites
The following are the pre-requisites in that order:

1. SAP Cloud Platform Global Account with Hyperledger Fabric Entitlement
2. Cloud Foundry enabled in one of your SAP Cloud Platform sub-accounts
3. Hyperledger Fabric node(s) service instance(s) already created in one of the spaces of the cloud foundry sub-account
4. A dev-channel plan cloud foundry service instance already created in a cloud foundry space
5. A service key already created to access the dev-channel cloud foundry service instance
