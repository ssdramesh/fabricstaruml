const fs = require("fs");
const path = require("path");
const spawn = require('child_process').spawn;
const configuration = require("./configuration");
const scriptName = '/setupBizNetFromStarUMLModel.sh';

class ChaincodeGenerator {

  constructor(settingsFile) {
    this.settingsFile = settingsFile;
  }

  generate(model) {
    var scriptPath = configuration.readScriptPathSetting(this.settingsFile);
    var script = scriptPath + scriptName;
    var child = spawn('bash', [script, model], {
      cwd: scriptPath,
      env: process.env
    });
    var that = this;
    child.stdout.on('data', function (data) {
      var generatedPath = data.toString();
      var projectName = path.basename(model, path.extname(model));
      configuration.saveGeneratedProjectPathSetting(that.settingsFile, projectName, generatedPath);
      app.dialogs.showInfoDialog("Business Network is generated at: " + generatedPath);
    });
    child.stderr.on('data', function (data) {
      app.dialogs.showErrorDialog(data.toString());
    });
    child.on('close', function (code) {
      // Do Nothing for now..
      // app.dialogs.showInfoDialog("Finished: " + code);
    });
  }

  drop(model) {
    var projectName = path.basename(model, path.extname(model));
    var projectFolder = configuration.readGeneratedProjectPathSetting(this.settingsFile, projectName);
    var that = this;
    var child = spawn('rm', ['-rf', projectFolder], {
      cwd: path.dirname(projectFolder),
      env: process.env
    });
    child.stdout.on('data', function (data) {
//    Nothing to do here..
//    No data comes out after remove!
    });
    child.stderr.on('data', function (data) {
      app.dialogs.showErrorDialog(data.toString());
    });
    child.on('close', function (code) {
      configuration.deleteProject(that.settingsFile, projectName);
      app.dialogs.showInfoDialog("Project folder of business network at: " + projectFolder + " was completely deleted");
    });
  }
}

function generate(model, settingsFile) {
  var chaincodeGenerator = new ChaincodeGenerator(settingsFile);
  chaincodeGenerator.generate(model);
}

function drop(model, settingsFile) {
  var chaincodeGenerator = new ChaincodeGenerator(settingsFile);
  chaincodeGenerator.drop(model);
}

exports.generate = generate
exports.drop = drop
